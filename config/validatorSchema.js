const Joi = require('joi');

const ValidateLogin = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30),
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
}).with('username', 'password');

const ValidateRegister = Joi.object().keys({
  username: Joi.string().min(3).max(30),
  password: Joi.string().required(),
  email: Joi.string().email()
}).with('username', 'password');

const ValidateGetUser = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required()
});

const ValidateUpdateUser = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30),
  bio: Joi.string(),
  profile_picture: Joi.string()
});

exports = module.exports = {
  ValidateLogin,
  ValidateRegister,
  ValidateGetUser,
  ValidateUpdateUser
};