const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const Debates = sequelize.define('debates', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
    description: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
    category: {
        type: Sequelize.INTEGER(11),
        allowNull: false
    },
    participants_limit: {
        type: Sequelize.INTEGER(),
        allowNull: false
    },
    entry_fees_crypto: {
        type: Sequelize.INTEGER(),
        allowNull: true
    },
    entry_fees_fiat: {
        type: Sequelize.INTEGER(),
        allowNull: true
    },
    to_prove_1: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
    to_prove_2: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
    start_time: {
        type: Sequelize.DATE,
        allowNull: false
    },
    end_time: {
        type: Sequelize.DATE,
        allowNull: false
    },
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {
    tableName: 'debates'
});

module.exports = Debates;