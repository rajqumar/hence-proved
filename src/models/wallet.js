const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const Wallet = sequelize.define('wallet', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER(11),
        allowNull: false
    },
    address: {
        type: Sequelize.STRING(125),
        allowNull: false,
    },
    private_address: {
        type: Sequelize.STRING(255),
        allowNull: false,
        unique: true
    },
    balance: {
        type: Sequelize.INTEGER(),
        allowNull: true
    },
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {
    tableName: 'wallet'
});

module.exports = Wallet;