const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const DebateWinners = sequelize.define('debate_winners', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    debate_id: {
        type: Sequelize.INTEGER(11),
        allowNull: false
    },
    winner_first_id: {
        type: Sequelize.INTEGER(),
        allowNull: false
    },
    winner_second_id: {
        type: Sequelize.INTEGER(),
        allowNull: true
    },
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {
    tableName: 'debate_winners'
});

module.exports = DebateWinners;