const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const DebateCategories = sequelize.define('debate_categories', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {
    tableName: 'debate_categories'
});

module.exports = DebateCategories;