const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const Users = sequelize.define('users', {
    id: {
      type: Sequelize.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      type: Sequelize.STRING(125),
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING(255),
      allowNull: false,
      unique: true  
    },
    bio: {
      type: Sequelize.STRING(255),
      allowNull: true,
    },
    profile_picture: {
      type: Sequelize.STRING(255),
      allowNull: true,
    },
    password: {
      type: Sequelize.STRING(255),
      allowNull: true
    },
    resetPasswordToken: {
      type: Sequelize.STRING(255),
      allowNull: true
    },
    resetPasswordExpires: {
      type: Sequelize.DATE,
      allowNull: true
    },
    emailRegisterToken: {
      type: Sequelize.STRING(255),
      allowNull: true
    },
    emailRegisterExpires: {
      type: Sequelize.DATE,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true
    }
  }, {
    tableName: 'users'
});

// Users.prototype.setPassword = function(password) {
//   this.salt = crypto.randomBytes(16).toString('hex');
//   this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
// };

// Users.prototype.validatePassword = function(password) {
//   const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
//   return this.hash === hash;
// };

// Users.prototype.generateJWT = function() {
//   const today = new Date();
//   const expirationDate = new Date(today);
//   expirationDate.setDate(today.getDate() + 60);

//   return jwt.sign({
//     email: this.email,
//     id: this._id
//   }, process.env.JWT_SECRET);
// }

// Users.prototype.toAuthJSON = function() {
//   return {
//     _id: this._id,
//     username: this.username,
//     email: this.email,
//     salt: this.salt,
//     user_id:this.id,
//     token: this.generateJWT(),
//   };
// };

module.exports = Users;