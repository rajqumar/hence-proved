const Sequelize = require('sequelize');
const sequelize = require('../../config/db');

const DebateParticpants = sequelize.define('debate_participants', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    debate_id: {
        type: Sequelize.INTEGER(11),
        allowNull: false
    },
    user_id: {
        type: Sequelize.INTEGER(),
        allowNull: false
    },
    points: {
        type: Sequelize.INTEGER(),
        allowNull: true
    },
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {
    tableName: 'debate_participants'
});

module.exports = DebateParticpants;