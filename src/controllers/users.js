const User = require('../models/users');
const Wallet = require('../models/wallet');
const {
  ValidationError,
  PermissionError,
  AuthorizationError,
  DatabaseError,
  NotFoundError,
  OperationalError
} = require('../utils/errors');
const Ether = require('../utils/crypto/ether');
const { Op } = require("sequelize");

const login = async (req, res, next) => {
  const username = req.parsed.username;
  let foundUser = null;
  try {
    foundUser = await User.findOne({ username });
  } catch (error) {
    return next(new AuthorizationError('Username or password do not match'));
  }

  if (foundUser === null || foundUser.password !== req.parsed.password) {
    return next(new NotFoundError('Username or password do not match'));
  }
  next();
};

const getUser = async (req, res) => {
  const username = req.params.username;
  try {
    const user = await User.findOne({ username });
    if (user === null) {
      return next(new NotFoundError('No user in the database'));
    }

    res.status(200).json({
      msg: 'User found successfully',
      result: user
    });
  } catch (err) {
    next(new DatabaseError('Error in fetching user Info'));
  }
};

const register = async (req, res, next) => {

  // check if user exists
  const existing_user = await User.findOne({ [Op.or]: [{ email: req.parsed.email }, { username: req.parsed.username }] });
  if (existing_user) {
    next(new DatabaseError('User already exists'));
  }

  const user = new User();
  const wallet = new Wallet();

  user.username = req.parsed.username;
  user.password = req.parsed.password;
  user.email = req.parsed.email;

  try {
    // register user
    await user.save();

    // create ethereum wallet
    const ether_wallet = new Ether(user.id);
    await ether_wallet.create();

    wallet.user_id = user.id
    wallet.address = ether_wallet.address;
    wallet.private_adress = ether_wallet.encPrivKey;

    // save wallet details
    await wallet.save();

    res.status(201).json({
      msg: 'User successfuly registered'
    });
  } catch (error) {
    await user.destroy();
    next(new DatabaseError('Error in fetching user Info'));
  }
};

const update = async (req, res) => {
  const username = req.parsed.username;
  let user = null;
  try {
    user = await User.findOne({ username });
  } catch (error) {
    res.status(400).json({
      msg: 'Some error occurred'
    });
  }
  if (user === null) {
    return next(new NotFoundError('No user in the database'));
  }

  user.bio = req.body.bio;
  user.profile_picture = req.body.profile_picture;
  try {
    await user.save();
    res.status(200).json({
      msg: 'User successfuly updated',
      user: user
    });
  } catch (error) {
    next(new DatabaseError('Could not update User'));
  }
};

module.exports = {
  login,
  getUser,
  update,
  register
};
