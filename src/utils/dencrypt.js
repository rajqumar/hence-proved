'use strict';

/** 
 * @todo Refactor later
*/
const crypto = require('crypto');
const { UB_ENC_SECRET, UB_ENC_SALT } = process.env;
const algorithm = 'aes-256-cbc';
const ITERATION=1000
const KEYSIZE=64
/**
 * @dev Function to encrypt plain text into cipher test
 * @param userId, Users Id used in creating Initialisation Vector
 * @param plain, Plain Text, to be encrypted
 */
const encrypt = (userId, plain) => {
  return new Promise((resolve, reject) => {
    if (!userId) {
      reject(new Error('UserId should not be null'));
    }
    try {
      let key = crypto.pbkdf2Sync(UB_ENC_SECRET, UB_ENC_SALT, ITERATION, KEYSIZE, 'sha1').toString('hex');
      let iv = crypto.pbkdf2Sync(`${userId}_${UB_ENC_SECRET}`, UB_ENC_SALT, ITERATION, KEYSIZE, 'sha1').toString('hex');
      const cipher = crypto.createCipheriv(algorithm, key.slice(0, 32), iv.slice(0, 16));
      let encrypted = cipher.update(plain, 'utf8', 'hex');
      encrypted += cipher.final('hex');
      resolve(encrypted);
    } catch (e) {
      reject(new Error(e));
    }
  });
};

/**
 * @dev Function to encrypt plain text into cipher test
 * @param userId, Users Id used in creating Initialisation Vector
 * @param cipher, Cipher Text, to be decrypted
 */
const decrypt = (userId, cipher) => {
  return new Promise((resolve, reject) => {
    if (!userId) {
      reject(new Error('UserId should not be null'));
    }
    try {
      let key = crypto.pbkdf2Sync(UB_ENC_SECRET, UB_ENC_SALT, ITERATION, KEYSIZE, 'sha1').toString('hex');
      let iv = crypto.pbkdf2Sync(`${userId}_${UB_ENC_SECRET}`, UB_ENC_SALT, ITERATION, KEYSIZE, 'sha1').toString('hex');
      const plain = crypto.createDecipheriv(algorithm, key.slice(0, 32), iv.slice(0, 16));
      let decrypted = plain.update(cipher, 'hex', 'utf8');
      decrypted += plain.final('utf8');
      resolve(decrypted);
    } catch (e) {
      reject(new Error(e));
    }
  });
};

module.exports = { encrypt, decrypt };