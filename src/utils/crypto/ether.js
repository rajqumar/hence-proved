const Web3 = require('web3');
// const BigNumber = require('bignumber.js');
const BN = require('../bignumber');
let web3;
if(process.env.NODE_ENV === 'production') {
  web3 = new Web3(process.env.ETHEREUM_NETWORK);
} else {
  web3 = new Web3(process.env.ETHEREUM_NETWORK_TEST);
}
const {encrypt, decrypt} = require('../dencrypt');
// const {coins} = require('../../../config/globalKeys');

class EtherUtils {
  constructor(coinDetails) {
    this.privateKey = null;
    this.fromAddress = null;
    this.toAddress = null;
    this.transferFromAddr = null;
    this.value = null;
    this.nonce = null;
    this.gasPrice = null;
    this.gas = null;
    this.coinDetails = coinDetails;
    this.decimals = coinDetails && coinDetails.decimals ? Number(coinDetails.decimals) : null;
    this.transaction = null;
    if(this.coinDetails && this.coinDetails['ABI'] && typeof this.coinDetails['ABI'] === 'string')
      this.coinDetails['ABI'] = JSON.parse(this.coinDetails['ABI']);
  }

  privateToAccount() {
    this.fromAddress = web3.eth.accounts.privateKeyToAccount(this.privateKey).address;
  }

  getNonce() {
    return web3.eth.getTransactionCount(this.fromAddress, 'pending');
  }

  getGasPrice() {
    return web3.eth.getGasPrice();
  }

  getEstimateGas() {
    return web3.eth.estimateGas(this.txnData);
  }

  async createTx(type=String) {

    this.gas = 21000;
    if(type === 'TOHW')
      // this.value = (new BigNumber(this.value)).minus((new BigNumber(this.gas)).multipliedBy(this.gasPrice)).toFixed(0);
      // this.value = BN.multiply(BN.subtract(this.value, this.gas), this.gasPrice);
      this.value = BN.subtract(this.value, BN.multiply(this.gas, this.gasPrice));

    const tx = {
      from: this.fromAddress,
      to: this.toAddress,
      value: this.value,
      gas: this.gas,
      gasPrice: this.gasPrice,
      nonce: this.nonce
    };

    return web3.eth.accounts.signTransaction(tx, this.privateKey);
  }

  async createERCtx(type=String, args=null) {
    const contractInstance = new web3.eth.Contract(this.coinDetails['ABI'], this.coinDetails['address']);
    let encodedABI;

    if(type == 'approve')
      encodedABI = contractInstance.methods.approve(this.toAddress, this.value).encodeABI();
    else if(type === 'transferFrom')
      encodedABI = contractInstance.methods.transferFrom(this.transferFromAddr, this.toAddress, this.value).encodeABI();
    else if(type === 'whitelist')
      encodedABI = contractInstance.methods.setWhitelist(this.toAddress, true).encodeABI();
    else if(type === 'bulkWhitelistTransferTimeLock')
      encodedABI = contractInstance.methods.bulkIssueWhitelistAndTimelock(this.transferFromAddr, this.toAddress, this.value, args.releaseTime).encodeABI();
    else
      encodedABI = contractInstance.methods.transfer(this.toAddress, this.value).encodeABI();

    if(!this.gasPrice) this.gasPrice = '90000000000';

    const tx = {
      from: this.fromAddress,
      to: this.coinDetails['address'],
      data: encodedABI,
      gasPrice: this.gasPrice,
      nonce: this.nonce
    };

    this.gas = await web3.eth.estimateGas({...tx});
    if(!this.gas) this.gas = 300000;
    tx.gas = this.gas;

    return web3.eth.accounts.signTransaction(tx, this.privateKey);
  }

  sendTx(signedTx) {
    return web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  }

  async checkAllowance(owner, spender){
    const contractInstance = new web3.eth.Contract(this.coinDetails['ABI'], this.coinDetails['address']);
    let allowed = await contractInstance.methods.allowance(owner, spender).call();
    allowed = BN.divide(allowed, (10 ** this.decimals));
    return allowed;
  }
}


class Ether extends EtherUtils{
  constructor(userId, coinDetails) {
    super(coinDetails);
    this.name = 'ETH';
    this.userId = userId;
    this.wallet = null;
    this.address = null;
    this.encPrivKey = null;
    this.balance = null;
  }

  async create() {
    const account = web3.eth.accounts.create();
    this.address = account.address;
    const privateKey = account.privateKey;
    this.encPrivKey = await encrypt(this.userId, privateKey);
  }

  async transfer(amount, toAddr, data) {
    this.encPrivKey = data.privateKey;
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = await decrypt(this.userId, this.encPrivKey);
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    return this.processETHtxn('TOHW');
  }

  async transferErc20(amount, toAddr, data) {
    this.encPrivKey = data.privateKey;
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = await decrypt(this.userId, this.encPrivKey);
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    return this.processERC20txn();
  }

  async approve(amount, toAddr, data) {
    this.encPrivKey = data.privateKey;
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = await decrypt(this.userId, this.encPrivKey);
    this.toAddress = toAddr;
    return this.processERC20txn('approve');
  }

  async transferFrom(amount, fromAddr, toAddr, nonce) {
    this.transferFromAddr = fromAddr;
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = process.env.LCX_WALLET_PRIVATE_KEY;
    this.toAddress = toAddr;
    return this.processERC20txn('transferFrom', nonce);
  }

  // withdrawal => from lcx hot wallets to user's provided address
  async withdrawal(amount, toAddr) {
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = process.env.LCX_HOT_WALLET_PRIVATE_KEY;
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    await this.processETHtxn();
    return;
  }

  // withdrawal => from lcx hot wallets to user's provided address
  async withdrawalErc20(amount, toAddr) {
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = process.env.LCX_HOT_WALLET_PRIVATE_KEY;
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    await this.processERC20txn();
    return;
  }

  // whitelist => whitelist any address via a whitelisting agent for a STO token
  async whitelistErc20Address(toAddr, nonce) {
    this.privateKey = process.env.LCX_WHITELIST_AGENT_PRIVATE_KEY;
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    await this.processERC20txn('whitelist', nonce);
    return;
  }

  // withdrawal fee for approval => from lcx wallet to user's address
  async transferApprovalFee(amount, toAddr, nonce) {
    // this.value = (new BigNumber(amount).multipliedBy(new BigNumber(10 ** data.decimals))).toFixed(0);
    this.value = BN.multiply(amount, (10 ** this.decimals));
    this.privateKey = process.env.LCX_WALLET_PRIVATE_KEY;
    this.toAddress = toAddr;
    this.gasPrice = await this.getGasPrice();
    await this.processETHtxn(undefined, nonce);
    return;
  }

  async bulkWhitelistTransferTimeLock(fromAddress, toAddresses, values, releaseTime ,nonce) {
    this.transferFromAddr = fromAddress;
    this.privateKey = process.env.LCX_WHITELIST_TIMELOCK_AGENT_PRIVATE_KEY;
    this.toAddress = toAddresses;
    this.value = [];
    for(let value of values){
      this.value.push(BN.multiply(value, (10 ** this.decimals)));
    }
    this.gasPrice = await this.getGasPrice();
    const args = {
      releaseTime: releaseTime
    };
    await this.processERC20txn('bulkWhitelistTransferTimeLock', nonce, args);
    return;
  }

  async processETHtxn(type=String, nonce) {
    this.privateToAccount();

    if(nonce)
      this.nonce = nonce;
    else
      this.nonce = await this.getNonce();

    let signedTransaction;
    if(type === 'TOHW')
      signedTransaction = await this.createTx(type);
    else
      signedTransaction = await this.createTx();

    this.transaction = web3.eth.sendSignedTransaction(signedTransaction.rawTransaction);
    return;
  }

  async processERC20txn(type=String, nonce, args=null) {
    this.privateToAccount();

    if(nonce)
      this.nonce = nonce;
    else
      this.nonce = await this.getNonce();    
      
    let signedTransaction;
    if(['approve', 'transferFrom', 'whitelist', 'bulkWhitelistTransferTimeLock'].includes(type))
      signedTransaction = await this.createERCtx(type, args);
    else
      signedTransaction = await this.createERCtx();

    this.transaction = web3.eth.sendSignedTransaction(signedTransaction.rawTransaction);
    return;
  }

  async checkBalance(data, wallet='user') {
    if(wallet === 'hot'){
      this.privateKey = process.env.LCX_HOT_WALLET_PRIVATE_KEY;
      this.privateToAccount();
    } else {
      this.encPrivKey = data.privateKey;
      this.userId = data.UserId;
      this.privateKey = await decrypt(this.userId, this.encPrivKey);
      this.privateToAccount();
    }

    if(this.name === 'ETH') {
      this.balance = await web3.eth.getBalance(this.fromAddress);
    } else {
      const contractInstance = new web3.eth.Contract(this.coinDetails['ABI'], this.coinDetails['address']);
      this.balance = await contractInstance.methods.balanceOf(this.fromAddress).call();
    }
  }
}

module.exports = Ether;