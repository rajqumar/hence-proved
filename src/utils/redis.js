// FIXME Add all the SET GET functions of Redis here
const asyncRedis = require("async-redis");
const client = asyncRedis.createClient();
 
client.on('connect', () => {
    console.log('Redis Connection Success');
});

client.on("error", function (err) {
    console.log("Redis Connection Error = " + err);
});

// client.set('DAG4MqGCBzcbav8tnbYLe3LgfaZj4W7AWaWyCWUt', 5)
// client.set('DAG42zYSKFV7mTMqetBrr5ebE5xcjD5jSXy3g4zQ', 7)

const GET = async (key) => {
    const result = await client.get(key)
    return result
}

const SET = async (key, value) => {
    const result = await client.set(key, value)
    return result
}

module.exports = {
    GET,
    SET
}


