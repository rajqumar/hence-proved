const express = require('express');
const debate = require('../controllers/debate');
const validate = require('../controllers/validators/debate_validator');
const { errorHandler } = require('../middlewares/errors');
const router = express.Router();
const jwt = require('../middlewares/jwt');

// create a debate
// fetch all debates
// fetch all debate categories

// if end_time > current_time
// fetch one debate => fetch debate details
// fetch one debate participants
// add participants => payment required (crypto or fiat)

// if end_time < current_time
// fetch one debate => fetch debate details
// fetch participants
// fetch winners => with winning amounts
// fetch badges and amount won
// fetch point proved

router.get('/:username', validate.getUser, user.getUser);
router.post('/register', validate.register, user.register);

router.post('/login', validate.login, user.login, jwt.sendToken);
router.put('/update', jwt.verifyToken, validate.updateUser, user.update);

router.use(errorHandler);

module.exports = router;
